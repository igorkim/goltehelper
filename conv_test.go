package goltehelper

import "testing"

func TestRbgConv(t *testing.T) {
	tables := []struct {
		nprb      int
		systemRbs int
		y         int
	}{
		{6, 6, 6},
		{25, 25, 13},
		{50, 50, 17},
		{100, 100, 25},
	}
	for _, table := range tables {
		result := RbsToRbgs(table.nprb, table.systemRbs)
		if result != table.y {
			t.Errorf("RBGs for %d RBs system bandwidth %d was incorrect, got: %d, want: %d.", table.nprb, table.systemRbs, result, table.y)
		}
	}
}

func TestPhyTp(t *testing.T) {
	tables := []struct {
		nprb int
		mcs  int
		ans  int64
	}{
		{6, 0, 152000},
		{6, 10, 936000},
		{6, 18, 1928000},
		{6, 28, 4392000},
		{25, 0, 680000},
		{25, 10, 4008000},
		{25, 18, 7992000},
		{25, 28, 18336000},
		{50, 0, 1384000},
		{50, 10, 7992000},
		{50, 18, 16416000},
		{50, 28, 36696000},
		{100, 0, 2792000},
		{100, 10, 15840000},
		{100, 18, 32856000},
		{100, 28, 75376000},
	}
	for _, table := range tables {
		result := int64(GetPHYThroughput(table.nprb, table.mcs, 1))
		if result != table.ans {
			t.Errorf("PHY throughput for %d RBs and MCS %d was incorrect, got: %d, want: %d.", table.nprb, table.mcs, result, table.ans)
		}
	}
}

func TestRbsConv(t *testing.T) {
	tables := []struct {
		rbgs      int
		systemRbs int
		y         int
	}{
		{6, 6, 6},
		{2, 25, 4},
		{3, 25, 6},
		{8, 25, 16},
		{13, 25, 25},
		{5, 50, 15},
		{17, 50, 50},
		{3, 100, 12},
		{25, 100, 100},
	}
	for _, table := range tables {
		result := RbgsToRbs(table.rbgs, table.systemRbs)
		if result != table.y {
			t.Errorf("RBGs for %d RBGs system bandwidth %d was incorrect, got: %d, want: %d.", table.rbgs, table.systemRbs, result, table.y)
		}
	}
}
